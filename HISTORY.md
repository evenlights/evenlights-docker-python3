=======
History
=======

1.1.1 (2018-06-17)
------------------
* pip3 -> pip symlink


1.1.0 (2018-06-17)
------------------
* `nginx` filed removed, use nginx image instead
* `uWSGI` files removed, install via pip instead
* server packages removed
* app directory moved to `/app`
* media and static dirs moved to `/app/static` and `/app/media` respectively


1.0.0 (2018-06-09)
------------------

Initial release. Support for uwsgi and nginx servers. 
