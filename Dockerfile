FROM ubuntu:18.04

MAINTAINER Evenlights Studio <development@evenlights.com>

ENV PATH /usr/local/bin:$PATH
ENV LANG C.UTF-8
ENV PYTHON_VERSION 3.6.5-3
ENV IMAGE_VERSION 1.1.0

RUN set -ex \
    && pythonDeps=" \
        python3 \
        python3-dev \
        python3-pip \
        python3-setuptools \
        python3-wheel \
    " \
    \
    && apt-get update \
    \
    && apt-get install -y tzdata apt-utils \
    && ln -fs /usr/share/zoneinfo/Etc/UTC /etc/localtime \
    && dpkg-reconfigure --frontend noninteractive tzdata \
    \
    && apt-get install -y --no-install-recommends $pythonDeps \
    \
    && rm -rf /var/lib/apt/lists/*

RUN cd /usr/bin \
	&& ln -s idle3 idle \
	&& ln -s pydoc3 pydoc \
	&& ln -s python3 python \
	&& ln -s python3-config python-config \
    && ln -s pip3 pip

RUN bash -c 'mkdir -p /app/{src,static,media}' \
    && chown -R www-data:www-data /app/static \
    && chown -R www-data:www-data /app/media
